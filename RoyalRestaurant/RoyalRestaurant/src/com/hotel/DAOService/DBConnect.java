package com.hotel.DAOService;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
	static Connection con;
	
	static Connection connection; 
	public static Connection openConnection() {
		String driverName = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:xe";
		String username = "prudhviraju";
		String password = "prudhviraju";
		
		try {
			Class.forName(driverName);
			con =  DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			System.out.println();
		}
		return con;
	}

	public static void closeConnection() {
		try{
		if (connection != null){
			connection.close();
		}
		}catch(SQLException e) {
            e.printStackTrace();
         }
	}
}

