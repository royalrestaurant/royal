package com.hotel.serviceImpl;

import java.util.List;

import com.hotel.bean.Item;
import com.hotel.service.IItem;

public class IItemImpl implements IItem {

	@Override
	public boolean addItem(Item item) {
		return false;
	}

	@Override
	public boolean updateItem(Item item) {
		return false;
	}

	@Override
	public boolean deleteItem(int itemNo) {
		return false;
	}

	@Override
	public List<Item> getAllItems() {
		return null;
	}

	@Override
	public List<Item> getItemsByCategory(String category) {
		return null;
	}

}
