package com.hotel.service;

import java.util.ArrayList;

import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;

public interface ItemServices {
	public void addItem(Item item);

	public void updateItem(Item item);

	public void deleteItem(int itemNumber);

	public ArrayList<Item> getAllItems();

	public ArrayList<Item> getItemsByCategory(String category) throws CategoryNotFoundException;
}
