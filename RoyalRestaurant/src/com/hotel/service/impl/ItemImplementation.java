package com.hotel.service.impl;

import java.util.ArrayList;

import com.hotel.DAO.ItemDAO;
import com.hotel.DAO.impl.ItemDAOImplementation;
import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;
import com.hotel.service.ItemServices;

public class ItemImplementation implements ItemServices {
	
	
		ItemDAO itemDAO= new ItemDAOImplementation();
	@Override
	public void addItem(Item item) {
		
		itemDAO.addItemtoDB(item);

	}

	@Override
	public void updateItem(Item item) {
		itemDAO.updateItemtoDB(item);

	}

	@Override
	public void deleteItem(int itemNumber) {
		itemDAO.deleteItemtoDB(itemNumber);

	}

	@Override
	public ArrayList<Item> getAllItems() {
		return itemDAO.getAllItemsfromDB();
		
	}

	@Override
	public ArrayList<Item> getItemsByCategory(String category) throws CategoryNotFoundException {
		return itemDAO.getItemsByCategoryfromDB(category);
	
	}

}
