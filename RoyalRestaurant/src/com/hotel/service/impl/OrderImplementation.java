package com.hotel.service.impl;

import java.util.ArrayList;

import com.hotel.DAO.OrderDAO;
import com.hotel.DAO.impl.OrderDAOImplementation;
import com.hotel.bean.Item;
import com.hotel.service.OrderServices;

public class OrderImplementation implements OrderServices {
	
	ArrayList<Item> items = new ArrayList<Item>();
	ArrayList<Integer> quantities = new ArrayList<Integer>();
	 String paymentMode;
	 int totalBill; 
     public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}

	public ArrayList<Integer> getQuantities() {
		return quantities;
	}

	public void setQuantities(ArrayList<Integer> quantities) {
		this.quantities = quantities;
	}


	
    
    OrderDAO orderImpl=new OrderDAOImplementation();
    public int getTotalbill() {
    	this.totalBill=totalBill();
 		return totalBill;
 	}

 	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	//public void setTotalbill(int totalbill) {
 		//this.totalbill = totalbill;
 	//}
	@Override
	public int totalBill() {
		int bill=0;
		for(int i=0;i<items.size();i++){
			bill+=items.get(i).getPrice()*quantities.get(i);
		}
		return bill;
	}

	@Override
	public int totalTimeRequired() {
		int time=0;
		for(int i=0;i<items.size();i++){
			time+=items.get(i).getCookingTime()*quantities.get(i);
		}
		return time;
		
	}

	@Override
	public void placeOrder() {
      orderImpl.addOrdertoDB(this);
      orderImpl.addFoodordertoDB(this);
	}
 

	public void displayOrder(){
    	for(int i=0;i<items.size();i++){
    	System.out.println(items.get(i));
    	System.out.println("quantity "+quantities.get(i));
    	}
    }
}
