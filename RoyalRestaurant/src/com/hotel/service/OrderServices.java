package com.hotel.service;

public interface OrderServices {
	
	public int totalBill();

	public int totalTimeRequired();

	public void placeOrder();
	public void displayOrder();
}
