package com.hotel.DAO.impl;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.hotel.DAO.ConnectionDAO;
import com.hotel.DAO.ItemDAO;
import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;

import java.sql.*;

public class ItemDAOImplementation implements ItemDAO {

	static Connection connection;
	static Logger logger = Logger.getLogger(ItemDAO.class);


	@Override
	public void addItemtoDB(Item item) {
		connection = ConnectionDAO.openConnection();
		String insertQuery = "Insert into ITEMS values(?,?,?,?,?,?)";
		PreparedStatement statement = null;
		PropertyConfigurator.configure("log4j.properties");
		try {
			statement = connection.prepareStatement(insertQuery);
			statement.setInt(1, item.getItemId());
			statement.setString(2, item.getName());
			statement.setInt(3, item.getPrice());
			statement.setInt(4, item.getCookingTime());
			statement.setString(5, item.getCategory());
			statement.setString(6, item.getAvailability());
			statement.execute();
			logger.info("Inserted Sucessfully");
		} catch (SQLException e) {
			logger.error(e.getMessage());
			
		}finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				logger.info("database closed");
				
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}

	}

	@Override
	public void updateItemtoDB(Item item) {
		connection = ConnectionDAO.openConnection();
		String updateQuery = "Update Items set item_name=?,item_price=?,cookingtime=?,item_category=?,item_availability=? where item_id=?";
		PreparedStatement statement = null;
				try {
			statement = connection.prepareStatement(updateQuery);
			statement.setString(1, item.getName());
			statement.setInt(2, item.getPrice());
			statement.setInt(3, item.getCookingTime());
			statement.setString(4, item.getCategory());
			statement.setString(5, item.getAvailability());
			statement.setInt(6,item.getItemId());
			statement.execute();
		   logger.info("updated the changes sucessfully");		
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				logger.info("database closed");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}


	}

	@Override
	public void deleteItemtoDB(int itemNumber) {
		connection = ConnectionDAO.openConnection();
		String deleteQuery = "update items set Item_availability='n' where item_id=?";
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(deleteQuery);
            statement.setInt(1, itemNumber);
			statement.execute();
			logger.info("deleted sucessfully");
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				logger.info("database closed");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}


	}

	@Override
	public ArrayList<Item> getAllItemsfromDB() {
       ArrayList<Item> itemList=new ArrayList<Item>();
       PreparedStatement statement=null;
		try{
			connection = ConnectionDAO.openConnection();
			String getQuery="Select * from ITEMS  where ITEM_AVAILABILITY='y' order by ITEM_CATEGORY";
			statement=connection.prepareStatement(getQuery);
			ResultSet rs=statement.executeQuery();
			logger.info("showing all the items");
			while(rs.next()){
				int itemId=rs.getInt(1);
				String itemName=rs.getString(2);
				int price=rs.getInt(3);
				int cookingTime=rs.getInt(4);
				Item item=new Item();
				item.setAvailability(rs.getString(6));
				item.setCategory(rs.getString(5));
				item.setCookingTime(cookingTime);
				item.setPrice(price);
				item.setItemId(itemId);
				item.setName(itemName);
				itemList.add(item);
			}
		}catch(SQLException e){
			logger.error(e.getMessage());
		}finally {
			try {
				if (statement != null)
				   statement.close();
				ConnectionDAO.closeConnection();
		//logger.info("database closed");
			} catch (SQLException e) {
				logger.error(e.getMessage());
			}
		}
		return itemList;
	}

	@Override
	public ArrayList<Item> getItemsByCategoryfromDB(String category)throws CategoryNotFoundException {
		ArrayList<Item> itemList=new ArrayList<Item>();
	       PreparedStatement statement=null;
			try{
				connection = ConnectionDAO.openConnection();
				statement=connection.prepareStatement("select * from Items where item_availability ='y' and item_category=?");
				statement.setString(1, category);
				ResultSet rs=statement.executeQuery();
				while(rs.next()){
					int itemId=rs.getInt(1);
					String itemName=rs.getString(2);
					int price=rs.getInt(3);
					int cookingTime=rs.getInt(4);
					Item item=new Item();
					item.setAvailability(rs.getString(6));
					item.setCategory(rs.getString(5));
					item.setCookingTime(cookingTime);
					item.setPrice(price);
					item.setItemId(itemId);
					item.setName(itemName);
					itemList.add(item);
				}
			}catch(Exception e){
				logger.error(e.getMessage());
			}finally {
				try {
					if (statement != null)
					   statement.close();
					ConnectionDAO.closeConnection();
					//logger.info("database closed");
				
				} catch (SQLException e) {
					logger.error(e.getMessage());
				}
			}
			if(itemList.size()==0)
			throw new CategoryNotFoundException("category not found");
			//logger.info("showing items by category");
			return itemList;
	}

}
