package com.hotel.DAO.impl;
import java.sql.*;
import java.text.*;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.hotel.DAO.ConnectionDAO;
import com.hotel.DAO.OrderDAO;
import com.hotel.bean.Item;
import com.hotel.service.impl.OrderImplementation;
public class OrderDAOImplementation implements OrderDAO{
	static Connection connection;
	static Logger logger = Logger.getLogger(ItemDAOImplementation.class);
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	int orderno;
	public void getSequenceNum(){
		connection = ConnectionDAO.openConnection();
		String sqlIdentifier = "select OrderNumber.NEXTVAL from dual";
		PreparedStatement pst;
		try {
			pst = connection.prepareStatement(sqlIdentifier);
			 ResultSet rs = pst.executeQuery();
			   if(rs.next())
			     orderno = rs.getInt(1);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	
		  
	}
	@Override
	public void addOrdertoDB(OrderImplementation obj){
		connection = ConnectionDAO.openConnection();
		String insertQuery="insert into orderdetails values(?,?,?,?)";
		PreparedStatement statement=null;
		getSequenceNum();
		try {
			statement = connection.prepareStatement(insertQuery);
			statement.setDouble(1,orderno);
			statement.setDate(2, new java.sql.Date(System.currentTimeMillis()));
			statement.setString(3,obj.getPaymentMode());
			statement.setInt(4, obj.getTotalbill());
			statement.executeQuery();
			logger.info("order inserted sucessfully");
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				logger.info("database closed");
				
			} catch (SQLException e) {
				logger.error(e.getMessage());
				
			}
		}
		
	}
	@Override
	public void addFoodordertoDB(OrderImplementation obj){
		String insertQuery="insert into foodorder values(?,?,?)";
		PreparedStatement statement=null;
		ArrayList<Item> items=obj.getItems();
		ArrayList<Integer> quantities=obj.getQuantities();
		
		for(int i=0;i<items.size();i++){
			connection = ConnectionDAO.openConnection();
		try {
			statement = connection.prepareStatement(insertQuery);
			statement.setDouble(1,orderno);
			statement.setInt(2,items.get(i).getItemId());
			statement.setInt(3,quantities.get(i));
			statement.executeQuery();
			
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}finally {
			try {
				if (statement != null)
					statement.close();
				ConnectionDAO.closeConnection();
				logger.info("database closed");
				
			} catch (SQLException e) {
				logger.error(e.getMessage());
				
			}
		}
		}
		
	}
}
