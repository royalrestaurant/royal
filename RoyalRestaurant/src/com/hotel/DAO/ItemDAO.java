package com.hotel.DAO;

import java.util.ArrayList;

import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;

public interface ItemDAO {
	public void addItemtoDB(Item item);

	public void updateItemtoDB(Item item);

	public void deleteItemtoDB(int itemNumber);

	public ArrayList<Item> getAllItemsfromDB();

	public ArrayList<Item> getItemsByCategoryfromDB(String category) throws CategoryNotFoundException;
 
}
