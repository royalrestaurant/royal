package com.hotel.exceptions;

public class CategoryNotFoundException extends Exception {

	public CategoryNotFoundException() {
		super();
	}

	public CategoryNotFoundException(String arg0) {
		super(arg0);
	}

}
