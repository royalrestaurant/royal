package com.hotel.exceptions;

public class ItemIdNotFoundException extends Exception {

	public ItemIdNotFoundException() {
		super();
	}

	public ItemIdNotFoundException(String message) {
		super(message);
	}

}
