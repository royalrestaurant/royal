package com.hotel.bean;

public class Item {
	String name;
	String category;
	int price;
	int cookingTime;
	String availability;
	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String string) {
		this.availability = string;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	int itemId;
	
	public Item() {
		super();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String i) {
		this.category = i;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getCookingTime() {
		return cookingTime;
	}
	public void setCookingTime(int cookingTime) {
		this.cookingTime = cookingTime;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", category=" + category + ", price=" + price + ", cookingTime=" + cookingTime
				+ ", availability=" + availability + ", itemId=" + itemId + "]";
	}

	
	
	
	

}
