package com.hotel.client;


import java.util.ArrayList;
import java.util.Scanner;

import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;
import com.hotel.service.impl.ItemImplementation;
import com.hotel.service.ItemServices;
import com.hotel.service.OrderServices;
import com.hotel.service.impl.OrderImplementation;



public class Customer {

	public static void main(String[] args) throws CategoryNotFoundException {

		System.out.println("welcome to the Royal resturant");
		Scanner sc = new Scanner(System.in);
		OrderServices order = new OrderImplementation();
		int choiceForExit = 0;
		ItemServices itemserv = new ItemImplementation();
		ArrayList<Item> itemListVeg = itemserv.getItemsByCategory("veg");
		ArrayList<Item> itemListNonVeg = itemserv.getItemsByCategory("non veg");
		ArrayList<Item> itemListBeverages = itemserv.getItemsByCategory("beverage");
		ArrayList<Item> items=((OrderImplementation)order).getItems();
		ArrayList<Integer> quantities=((OrderImplementation)order).getQuantities();
		//ArrayList<Integer> quantities=new ArrayList<Integer>();
		while (choiceForExit != 4) {
			System.out.println("enter the category");
			System.out.println("1. veg");
			System.out.println("2. nonveg");
			System.out.println("3. beverages");
			System.out.println("4. thats all for today.");
			System.out.println("enter ur choice");
			int choice = sc.nextInt();
			int itemId, quantity;
			switch (choice) {
			case 1:
				for (Item item : itemListVeg) {
					System.out.println(item);
				}
				System.out.println("Enter item id");
				itemId = sc.nextInt();


				boolean flag = false;

				for (Item item : itemListVeg) {
					if (itemId == item.getItemId()) {
						items.add(item);
						flag = true;
						break;
					}
				}
				if (flag) {
					System.out.println("Enter Quantity");
					quantity = sc.nextInt();
					quantities.add(quantity);
				} else
					System.out.println("Enter proper ItemId");
				break;
			case 2:// nonveg case
				for (Item item : itemListNonVeg) {
					System.out.println(item);
				}
				System.out.println("Enter item id");
				itemId = sc.nextInt();

				flag = false;

				for (Item item : itemListNonVeg) {
					if (itemId == item.getItemId()) {
						items.add(item);
						flag = true;
						break;
					}
				}
				if (flag) {
					System.out.println("Enter Quantity");
					quantity = sc.nextInt();
					quantities.add(quantity);
				} else
					System.out.println("Enter proper ItemId");
				break;
			case 3:// beverage case
				for (Item item : itemListBeverages) {
					System.out.println(item);
				}
				System.out.println("Enter item id");
				itemId = sc.nextInt();

				flag = false;

				for (Item item : itemListBeverages) {
					if (itemId == item.getItemId()) {
						items.add(item);
						flag = true;
						break;
					}
				}
				if (flag) {
					System.out.println("Enter Quantity");
					quantity = sc.nextInt();
					quantities.add(quantity);
				} else
					System.out.println("Enter proper ItemId");
				break;
			case 4: // exit case
				System.out.println("thankyou for stopping by");
				choiceForExit = 4;
				break;
			}
		}
        
		OrderImplementation orderImpl=(OrderImplementation)order;
		//orderImpl.setItems(items);
		//orderImpl.setQuantities(quantities);
		System.out.println("Your Order");
		order.displayOrder();
		System.out.println("Cooking Time");
		System.out.println(order.totalTimeRequired());
		System.out.println("Total Bill");
		System.out.println(orderImpl.getTotalbill());
		System.out.println("do u want to place order");
		System.out.println("1.yes");
		System.out.println("2.no");
		int ch = sc.nextInt();
		switch (ch) {
		case 1:
			System.out.println("1.cash");
			System.out.println("2.card");
			int pay = sc.nextInt();
			if (pay == 1) {
				orderImpl.setPaymentMode("cash");
			} else {
				orderImpl.setPaymentMode("card");
			}
			System.out.println("cashpaid");
			System.out.println("enjoy ur meal");
			order.placeOrder();
		case 2:
			order = null;
			System.out.println("Thank u Visit Again");
			break;
		}
		sc.close();
	}

}
