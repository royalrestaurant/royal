package com.hotel.client;

import java.util.ArrayList;
import java.util.Scanner;

import com.hotel.bean.Item;
import com.hotel.exceptions.CategoryNotFoundException;
import com.hotel.exceptions.ItemIdNotFoundException;
import com.hotel.service.ItemServices;
import com.hotel.service.impl.ItemImplementation;

public class Admin {

	public static void main(String[] args) throws ItemIdNotFoundException {
    
    
		System.out.println("welcome admin");
		Scanner scanner= new Scanner(System.in);
		int choiceForExit=0;
		ArrayList<Item> itemList=null;
		
		ItemServices itemImpl=new ItemImplementation();
		while(choiceForExit!=6){
			System.out.println("enter the choice number");
			System.out.println("1. get the list of all items");
			System.out.println("2. get the list of items by category");
			System.out.println("3. update an item information");
			System.out.println("4. set an item as unavailable.");
			System.out.println("5. add a new item");
			System.out.println("6. exit ");
			int choice= scanner.nextInt();
			
			switch(choice){
			case 1:
				itemList=itemImpl.getAllItems();
			     for(Item item1:itemList){
			    	 System.out.println(item1);
			     }
				break;
			case 2:
				System.out.println("enter the category name");
				String category=scanner.next();
				try{
				 itemList=itemImpl.getItemsByCategory(category);
				 for(Item item1:itemList){
			    	 System.out.println(item1);
			     }
				}catch(CategoryNotFoundException e){
					System.out.println("Category Not Found");
				}
			     
				break;
			case 3:
				itemList=itemImpl.getAllItems();
				for(Item item1:itemList){
			    	 System.out.println(item1);
			     }
				Item item= new Item();
				System.out.println("enter the itemid");
				int itemId=scanner.nextInt();
				boolean flag=false;
				for(Item item1:itemList){
					if(item1.getItemId()==itemId)
						flag=true;
				}
				try{
				if(!flag){
					throw new ItemIdNotFoundException("ItemIdNotFound");
				}
				item.setItemId(itemId);
				scanner.nextLine();
				System.out.println("enter the item name");
				String itemName=scanner.nextLine();
				item.setName(itemName);
				System.out.println("enter the item price");
				int itemPrice=scanner.nextInt();
				item.setPrice(itemPrice);
				System.out.println("enter the item cooking time");
				int itemCookingTime=scanner.nextInt();
				item.setCookingTime(itemCookingTime);
				System.out.println("enter the item category");
				String itemCategory=scanner.next();
				item.setCategory(itemCategory);
				System.out.println("enter the item availability");
				String itemAvailability=scanner.next();
				
				item.setAvailability(itemAvailability);
			     itemImpl.updateItem(item);
				}catch(ItemIdNotFoundException e){
					System.out.println("ItemIdNotFound");
				}
				break;
			case 4:
				itemList=itemImpl.getAllItems();
				for(Item item1:itemList){
			    	 System.out.println(item1);
			     }
				System.out.println("enter the item id");
				int id= scanner.nextInt();
				flag=false;
				for(Item item1:itemList){
					if(item1.getItemId()==id)
						flag=true;
				}
				try{
				if(!flag){
					throw new ItemIdNotFoundException("ItemIdNotFound");
				}
				
				itemImpl.deleteItem(id);
				}catch(ItemIdNotFoundException e){
				System.out.println("ItemId not found");	
				}
				break;
			case 5:
				Item newitem= new Item();
				System.out.println("enter the itemid");
				int itemId1=scanner.nextInt();
				newitem.setItemId(itemId1);
				scanner.nextLine();
				System.out.println("enter the item name");
				String itemName1=scanner.nextLine();
				newitem.setName(itemName1);
				System.out.println("enter the item price");
				int itemPrice1=scanner.nextInt();
				newitem.setPrice(itemPrice1);
				System.out.println("enter the item cooking time");
				int itemCookingTime1=scanner.nextInt();
				newitem.setCookingTime(itemCookingTime1);
				scanner.nextLine();
				System.out.println("enter the item category");
				String itemCategory1=scanner.nextLine();
				newitem.setCategory(itemCategory1);
				System.out.println("enter the item availability");
				String itemAvailability1=scanner.next();
				newitem.setAvailability(itemAvailability1);
				itemImpl.addItem(newitem);
				break;
			case 6:
				System.out.println("thankyou for stopping by");
				choiceForExit=6;
				break;
			default:
					System.out.println("wrong choice");
					break;
			}
		}
		scanner.close();
	}
}
